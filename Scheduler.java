import java.util.*;


public class Scheduler {

	private ArrayList<Server> servers;
	private int queues;
	
	public Scheduler(int queues)
	{
		   servers=new ArrayList<Server>();
		   this.queues=queues;
		if(this.queues==0)
			System.out.println("We don't have other  queues!");
		else
			for(int i=1;i<=queues;i++)
		{
			Server server1=new Server();
			servers.add(server1);
			Thread newThread=new Thread(server1);
			newThread.start();
			
		}	}
	
	 public boolean allServersFree()
	 {
		   if(servers.size()==0)
			  System.out.println("We don't have servers");
		   for(Server s: servers)
			 if(getFreeServer(s)==false) {
				 System.out.println("Not all servers are empty!");
			     return false;}
		   return true;
			
	 }

	public boolean getFreeServer(Server s)
	{
		   
		if(s.getNoOfClients()==0) {
			return true ;
		
		}
		else
		{
		  return false;
		}
		
		
	}
	public void dispatchClient(Client client1) {
		Server server1=servers.get(0);
		int min=Integer.MAX_VALUE;
		for(Server s:servers) {
			if(s.getWPeriod().get()<min)
			{
				server1=s;
				min=server1.getWPeriod().get();
			}
			server1.addClient(client1);
	}
	
		
	}
	public ArrayList<Server> getServers()
	{
		return servers;
	}
	

	public String toStringS()
	{String serverString="Servers :  ";
	 int indexServer=1;
	    for (Server s : servers) {
	    	if(s.getNoOfClients()==0)
	    		serverString=serverString + " \n server " + indexServer + " este gol. \n";
	    	else
	    	serverString=serverString + " \n  Server " + indexServer + " : " + s.toStrClients() + " \n"; 
	    	indexServer++;
	    }
	    return serverString;
	}
		
	}


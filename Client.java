
public class Client {
 private int id;
 private int tArrival;
 private int tService;
 
 public Client(int id,int t1,int t2)
   {  this.id=id;
     this.tArrival=t1;
     this.tService=t2;
     
	 
 }
 
   public int getId()
   {
	   return this.id;
	  
   }
   
   public int getTArrival()
   {
	   return this.tArrival;
	   
   }
   public int getTService()
   {
	   return this.tService;
   }
 
public void setId(int id)
{
	this.id=id;
}
public void setTArrival(int t1)
{
	this.tArrival=t1;
	
}

public void setTService(int t2)
{
   this.tService=t2;
}

public String toString(Client c)
{
	String returnString;
	returnString="( " + c.getId() + " , " + c.getTArrival() + " , " + c.getTService() + ")";
	return returnString;
	}
}
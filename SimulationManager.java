import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class SimulationManager implements Runnable {

private Scheduler scheduler;
private FileWriter fileWriter;
private PrintWriter printWriter;
private Integer parametersGiven[]=new Integer[7];
private String outputFile;
public ArrayList<Client> createdClients;

public SimulationManager(String s1,String s2)
{
     this.parametersGiven=readFromFile(s1);
     this.outputFile=s2;
     this.scheduler = new Scheduler(parametersGiven[1]);
      this.createdClients=createClients(parametersGiven[0],parametersGiven[3],parametersGiven[4],parametersGiven[5],parametersGiven[6]);
      

}


public Integer[] readFromFile(String filePath)
{    
	Integer paramFromFile[]=new Integer[7];
	int k=0;
	File parametersFile=new File(filePath);
	String paramFile;
     try {
		@SuppressWarnings("resource")
		Scanner scanFile=new Scanner(parametersFile);
		while(scanFile.hasNextLine()) {
	paramFile=scanFile.nextLine();
	if(paramFile.contains(","))
	{ String aux[];
	    aux=paramFile.split(",");
	    paramFromFile[k++]=Integer.parseInt(aux[0]);
	    paramFromFile[k++]=Integer.parseInt(aux[1]);
	    
	    
	}
	else
	{
	    paramFromFile[k++]=Integer.parseInt(paramFile);
		}
			
	}} catch (FileNotFoundException e) {
	   System.out.println("Eroare la citirea din fisier!");
		e.printStackTrace();
	}

     return paramFromFile;
     
}



public ArrayList<Client> createClients(int noOfClients,int tArMin,int tArMax,int tSmin,int tSmax)

{
	ArrayList<Client> randomClients=new ArrayList<Client>();
	    for(int i=0;i<noOfClients;i++) {
	    	Integer param[]=new Integer[3];
	    	param=generateClientParameters(tArMin,tArMax,tSmin,tSmax);
	    	Client randomClient=new Client(param[0],param[1],param[2]);
	    	randomClients.add(randomClient);
	    	 Comparator<Client> clientsComparator = new TimeComparator();
	            randomClients.sort(clientsComparator);
	    	
	    }
	    
	    return randomClients;
}
	   
	    public String toString(ArrayList<Client> clients)
	    {
	    	String str=new String("Clients :");
	    	for(Client c:clients)
	    	   str=str+ c.toString(c) + "\n ";
	    	return str;
	   }

public Integer[] generateClientParameters(int tArrivalMin,int tArrivalMax,int tServiceMin,int tServiceMax)
{
	  Integer[] parameters=new Integer[3];
	  Random random1=new Random();
	  parameters[0]=random1.nextInt(1000);
	  parameters[1]=random1.nextInt(tArrivalMax);
	    while(parameters[1]<tArrivalMin)
	    	parameters[1]=random1.nextInt(tArrivalMax);
	    parameters[2]=random1.nextInt(tServiceMax);
	    while(parameters[2]<tServiceMin)
	    	parameters[2]=random1.nextInt(tServiceMax);
	    return parameters;
	    	
}

@Override
public void run() {
	 try {
		fileWriter = new FileWriter(outputFile);
	} catch (IOException e1) {
	   System.out.println("Eroare fisier");
		e1.printStackTrace();
	}
     printWriter = new PrintWriter(fileWriter);
	 int time=0;
	 if(createdClients.size()==0)
	 {
		 System.out.println("No more clients!");
		
	 }
	 
	 
	 while(time <parametersGiven[2] && (scheduler.allServersFree()==true && createdClients.size()!=0) )
	 {
		
	     List<Client> clients2=new ArrayList<>();
			 if(createdClients.size()>0) {
			 if(createdClients.get(0).getTArrival()==time)
			 {
				 for(Client cl : createdClients)
					 if (cl.getTArrival()==time)
					 {
						 clients2.add(cl);
						 scheduler.dispatchClient(cl);
					 }else break;
			 }
			 
			 if(clients2.size()!=0)
			 {
				 for(Client c : clients2)
				 {
					 createdClients.remove(c);
				 }
			 }
		 }
			 
			 
		 
		 printWriter.println("Time : " + time ); 
		
			printWriter.println("Clients waiting:" + toString(createdClients)   );
			printWriter.println(  scheduler.toStringS());
			
			for (Server s: scheduler.getServers())
			{
				if(s.getNoOfClients()>0)
					s.timeDecrease();
			}
		time=time+1; 
			 try {
				Thread.currentThread();
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			
				e.printStackTrace();
			}
	 }
		
	
	for(Server s: scheduler.getServers())
     {
         s.setFlag(false);
     }
	 printWriter.close();	
	
}
}

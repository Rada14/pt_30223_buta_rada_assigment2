
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
private ArrayBlockingQueue<Client> clients;
private AtomicInteger waitingPeriod;
private boolean openQueue;
private boolean flag=true;

 public void timeDecrease()
 {
	 if(waitingPeriod.get()>0)
	 {		  
		  Client client=clients.element();
	      client.setTService(client.getTService()-1);
		  waitingPeriod.decrementAndGet();
	
	 }
 }
 
 public String toStrClients()
 {
	  String s=" ";
	 for(Client c : clients)
		 s=s+c.toString(c);
	 return s;
		  
 }
 
public AtomicInteger getWPeriod()
{
	return this.waitingPeriod ;
}
public Server() {
	this.clients=new ArrayBlockingQueue<Client>(1000);
	this.setOpenQueue(false);
	this.waitingPeriod =new AtomicInteger();
}


public void removeClient(Client c1)
{
	clients.remove(c1);
}
public void addClient(Client c1)
{
	this.clients.add(c1);
	this.setOpenQueue(true);
	int finalTime=c1.getTService()+waitingPeriod.get();
	this.waitingPeriod.lazySet(finalTime);
}


@Override
public void run() {
	while(flag==true) {
		try {
		if (!clients.isEmpty()) 
		{
			Client client1=clients.element();
			clients.remove();
			Thread.currentThread();
			Thread.sleep((client1.getTService())*1000);
				
		}else
		{
			 setOpenQueue(false); 
			 Thread.currentThread();
			Thread.sleep(1000);
			}
		}catch(InterruptedException e) {
			System.out.println("InterruptedException!");
			
			
		}
	}
	
}

public void setFlag(Boolean b)
{
	this.flag=b;
}


public int getNoOfClients()
{
	return this.clients.size();
}
			
	public String toString()
	{
		String clienti="Clienti :";
		for(Client c:clients)
		{ clienti=clienti + c.toString() + " ";
		
		}
		return clienti;
	}
	
	public ArrayBlockingQueue<Client> getClients()
	{
		return clients;
	}

	public boolean isOpenQueue() {
		return openQueue;
	}

	public void setOpenQueue(boolean openQueue) {
		this.openQueue = openQueue;
	}

		
	}




